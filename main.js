import Vue from 'vue'
import App from './App'
import uView from "uview-ui";

Vue.use(uView);

// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare)


Vue.config.productionTip = false;
// Vue.prototype.siteBaseUrl = 'http://124.71.202.247:8080';//测试地址
Vue.prototype.siteBaseUrl = 'https://api.wohuashi.com'
Vue.prototype.sessionKey = "access_token";

Vue.prototype.sendRequest = function(param) {
	var _self = this,
		url = param.url,
		method = param.method,
		header = {},
		data = param.data || {},
		token = "",
		hideLoading = param.hideLoading || false;

	//拼接完整请求地址
	var requestUrl = _self.siteBaseUrl + url;
	//固定参数:仅仅在小程序绑定页面通过code获取token的接口默认传递了参数token = login
	if (!data.token) { //其他业务接口传递过来的参数中无token
		token = uni.getStorageSync(_self.sessionKey); //参数中无token时在本地缓存中获取
		console.log("当前token:" + token);
	}

	//请求方式:GET或POST(POST需配置header: {'content-type' : "application/x-www-form-urlencoded"},)
	if (method) {
		// 'Bearer '
				method = method.toUpperCase(); //小写改为大写
				if (method == "POST") {
					header = {
						'content-type': "application/json",
						'Authorization':token
					};
				} else {
					header = {
						'content-type': "application/json",
						'Authorization':token
					};
				}
	} else {
		// method = "GET";
		header = {
			'content-type': "application/json",
			'Authorization': token
		};
	}
	//用户交互:加载圈
	if (!hideLoading) {
		uni.showLoading({
			title: '加载中...'
		});
	}

	console.log("网络请求start");
	//网络请求
	uni.request({
		url: requestUrl,
		method: method,
		header: header,
		data: data,
		success: res => {
			console.log(res);
			if (res.statusCode && res.statusCode != 201 && res.statusCode != 200) { //api错误
				if (res.statusCode == 401) {
					uni.showModal({
						content: "身份认证失败,点击确定重新操作",
						success: function(res) {
							if (res.confirm) {
								uni.navigateBack({
									delta: 1
								})
								 _self.login();
							}
						}
					});
				} else if (res.statusCode == 423) {
					uni.showModal({
						content: "错误的数据请求",
						success: function(res) {
							if (res.confirm) {
								uni.navigateBack({
									delta: 1
								})
							}
						}
					});
				} else if (res.statusCode == 417) {
					uni.showModal({
						content: " 数据错误，请联系管理员",
						success: function(res) {
							if (res.confirm) {
								uni.navigateBack({
									delta: 1
								})
							}
						}
					});
				} else if (res.statusCode == 500) {
					uni.showModal({
						content: " 服务器内部错误，请联系管理员",
						success: function(res) {
							if (res.confirm) {
								uni.navigateBack({
									delta: 1
								})
							}
						}
					});
				} else {
					uni.showModal({
						content: "错误，请联系管理员",
						success: function(res) {
							if (res.confirm) {
								uni.navigateBack({
									delta: 1
								})
							}
						}
					});
				}
			
				return;
			}
			typeof param.success == "function" && param.success(res.data);
		},
		fail: (e) => {
			uni.hideLoading();
			uni.showModal({
				content: "" + e.errMsg
			});
			typeof param.fail == "function" && param.fail(e.data);
		},
		complete: () => {
			uni.hideLoading();
			console.log("网络请求complete");
			if (!hideLoading) {
				uni.hideLoading();
			}
			typeof param.complete == "function" && param.complete();
			return;
		}
	});
}
// 检查登录态是否过期	
Vue.prototype.checkSession = function() {
	var that = this;
	uni.checkSession({
		success(res) {
			console.log(res)
			//session_key 未过期，并且在本生命周期一直有效
			console.log(new Date()+"======session正常")
			// Vue.prototype.login();
		},
		fail: async (err) => {
			// session_key 已经失效，需要重新执行登录流程
			console.log(new Date()+"======session失效")
			Vue.prototype.login();
		}
	})
}

// 全局获取token
Vue.prototype.login = function() {
	console.log("login方法")
	var _self = this;
	uni.login({
		success: function(res) {
			var icode = res.code;
			console.log(icode)
			uni.request({
				url: _self.siteBaseUrl + '/api/v1/weapp/authorizations',
				method: 'POST',
				data: {
					code: icode,
				},
				success: res => {
					console.log(res)
					if (res.errMsg == "request:ok") { //用户存在
						uni.setStorageSync('access_token', res.data.access_token);
						uni.setStorageSync('bound_phone', res.data.bound_phone);
					}
				},
				fail: (e) => {
					console.log("getMachineNum fail:" + JSON.stringify(e));
				},
				complete: () => {}
			});
		},
		fail: function(e) {
			console.log("微信login接口调用失败:" + JSON.stringify(e));
		}
	});
	return;
}

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
