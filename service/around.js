var http = require('./http.js');

/**
 * 获取周边店铺
 * lat
 * lng
 * page 
 * limit
 */ 
const getAroundStoreList = (params) => {
  const requestData = {
    url: '/data/periphery/shop/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}


module.exports = {
	getAroundStoreList: getAroundStoreList,
}