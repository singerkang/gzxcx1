var http = require('./http.js');

/**
 * 生成订单 
 */ 
const orderSubmit = params => {
  const requestData = {
    url: '/order/submit',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 生成秒杀订单
 */
const orderKillSubmit = params => {
  const requestData = {
    url: '/order/kill/submit',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 生成拼团订单
 */
const orderGroupSubmit = (params, groupId) => {
  const requestData = {
    url: '/order/group/startGroup?groupId=' + groupId,
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 *  参团
 */
const joinGroup = (params, groupRecordId) => {
  const requestData = {
    url: '/order/group/joinGroup?groupRecordId=' + groupRecordId,
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 生成砍价订单 
const orderBargainSubmit = params => {
  const requestData = {
    url: '/order/haggle/submit',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
 
// 去支付砍价订单
const orderBargainPay = params => {
  const requestData = {
    url: '/order/haggle/order',
    method: 'post',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
}; 

 // 砍价订单详情
const getBargainDetail = params => {
  const requestData = {
    url: '/order/haggle/item',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 砍价
const bargainOrderCut = params => {
  const requestData = {
    url: '/order/haggle/cut',
    method: 'put',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
}; 
// 砍价列表
const bargainOrderList = params => {
  const requestData = {
    url: '/order/haggle/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
 
 

/**
 * 追加小费 
 */ 
const addGratMoney = params => {
  const requestData = {
    url: '/order/tipPay',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 获取全部订单
const getOrderList = params => {
  const requestData = {
    url: '/order/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 删除订单 
 * 
 */ 
const orderDelete = params => {
  const requestData = {
    url: `/order/item?orderNo=${params.orderNo}`,
    method: 'delete',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

const getRefundList = params => {
  const requestData = {
    url: '/order/refund/page',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 订单详情
const getOrderDetail = params => {
  const requestData = {
    url: '/order/item',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 订单急速退款
const orderRefundFast = params => {
  const requestData = {
    url: `/order/refund/fast?orderNo=${params.orderNo}&type=${params.type}`,
    method: 'put',
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 订单申请退款 
const orderRefundNormal = params => {
  const requestData = {
    url: `/order/refund/normal?orderNo=${params.orderNo}&reason=${params.reason}`,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 获取退款详情
const getRefundDetails = params => {
  const requestData = {
    url: '/order/refund/' + params,
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 


// 获取订阅消息模板
const getMessageTemplate = params => {
  const requestData = {
    url: '/data/index/mini/notice',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 支付定单 
const roderPay = params => {
  const requestData = {
    url: '/order/pay',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 支付成功回调
const orderNotice = params => {
  const requestData = {
    url: '/order/notice',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 订单预提交
const orderPrePay = params => {
  const requestData = {
    url: '/order/preSubmit',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};




/**
 * 扫码支付
 * params: {
 *  "payFee": 0,  金额
    "platform": "WECHAT_MINI", 设备类型
    "store": "string", 店铺id
    "type": "FOOD", 订单类型
    "user": "string" 用户id
    "cost_use": "FOODS_ORDER",
    "device_info": "string",
    "open_id": "string",
    "payFee": 0,
    "pay_type": "ALIPAY",
    "trade_type": "string",

 * }
 */

const offlinePay = params => {
  const requestData = {
    url: '/order/offline/pay',
    method: 'POST',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * /form/comment 
 */ 

const commentOrder = params => {
  const requestData = {
    url: '/form/comment',
    method: 'POST',
    data: {
			formList: params
		}
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///data/store
// 获取商家logo
const bussLogo = params => {
  const requestData = {
    url: '/data/store',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 获取骑手轨迹
const getEmapTrack = params => {
  const requestData = {
    url: '/emap/delivery/track',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
module.exports = {
	orderSubmit,
	orderKillSubmit,
	orderGroupSubmit,
	joinGroup,
	orderBargainSubmit,
	orderBargainPay,
	getBargainDetail,
	bargainOrderCut,
	bargainOrderList,
	addGratMoney,
	getOrderList,
	orderDelete,
	getRefundList,
	getOrderDetail,
	getMessageTemplate,
	roderPay,
	orderNotice,
	offlinePay,
	orderRefundFast,
	getRefundDetails,
	orderRefundNormal,
  commentOrder,
  bussLogo,
  getEmapTrack,
  orderPrePay
}