var http = require('./http.js');


/**
 *  发送短信
 * params {
 * 	mobile   
 * }
 */
const getMobileCode = params => {
	const requestData = {
		url: '/selective/selShop/sendSmsForShop?mobile=' + params.mobile,
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 *  七牛上传图片获取token
 * params {
 * 	id   
 * }
 */
const getQiniuToken = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/token?id=' + params.id,
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 *  删除图片
 * params {
 * 	key   
 * }
 */
const deleteQiniuImage = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/delete?key='+params.key,
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
/**
 *  上传图片
 * params {
 * 	key   
 * }
 */
const uploadImage = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/upload',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}



/**
 *  店铺注册信息保存
 * params {
 * }
 */
const saveShopRegInfo = params => {
	const requestData = {
		url: '/selective/selShop/regInfo',
		method: 'put',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}


module.exports = {
	getMobileCode,
	getQiniuToken,
	deleteQiniuImage,
	uploadImage,
	saveShopRegInfo
}