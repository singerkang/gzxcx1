var http = require('./http.js');
///member/check
const membercheck = params => {
	const requestData = {
		url: '/member/check',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
// 获取用户 open_id
const getUserOpenId = params => {
  const requestData = {
    url: '/wxApp/code2Session',
    method: 'get',
    data:{
      code:params.code
    } 
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
const getQqOpenId = params => {
  const requestData = {
    url: '/third/qq_mini/code2Session',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
const memberverify = params => {
	const requestData = {
		url: '/member/verify',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
// 获取用户 UnionId
const getUserUnionId = params => {
  const requestData = {
    url: '/wxApp/userInfo',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 用户意见反馈
const userOpinion = params => {
  const requestData = {
    url: '/data/user/opinion',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 修改手机号码
const updataPhone = params => {
  const requestData = {
    url: '/data/user/phone?phone=' + params.phone + '&code=' + params.code,
    method: 'put' // data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
/** 
 * 获取用户信息
 */
const getUserInfo = params => {
  const requestData = {
    url: '/data/user',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/** 
 * 修改用户昵称 /data/user/nickname
 */
const changeNickName = params => {
  const requestData = {
    url: '/data/user/nickname?nickname=' + params.nickname,
    method: 'put',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/** 
 * 修改用户性别 data/user/gender?gender=1
 */
const changeGender = params => {
  const requestData = {
    url: '/data/user/gender?gender=' + params.gender,
    method: 'put',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
module.exports = {
  membercheck,
  memberverify,
	getUserOpenId,
	getUserUnionId,
	getQqOpenId,
	userOpinion,
	updataPhone,
	getUserInfo,
	changeNickName,
	changeGender
}



