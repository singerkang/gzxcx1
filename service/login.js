var http = require('./http.js');

// 根据手机号发送验证码,手机号码必须未注册
const getCode = (params) => {
  const requestData = {
    url: '/admin/new/sms/member/other',
    method: 'post',
    data: params
  }
  return http.fetFormHeaderApi(requestData).then(res => res.data);
}

const getPhoneCode = (params) => {
  const requestData = {
    url: '/admin/member_v2/sms',
    method: 'post',
    data: params
  }
  return http.fetFormHeaderApi(requestData).then(res => res.data);
}
const loginByPhone = params => {
  const requestData = {
    url: '/auth/oauth/member/mobile',
    method: 'post',
    data: params
  };
  return http.loginApi(requestData).then(res => res.data);
};

// 授权注册
const loginAuthorized = params => {
  const requestData = {
    url: '/data/member/register',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};  
 
 // 用户Unionid登录
const loginByUnionid = params => {
	const requestData = {
		url: '/auth/oauth/member/slat',
		method: 'post',
		data: params
	};
	return http.loginApi(requestData).then(res => res.data);
};

module.exports = {
	getCode,
	getPhoneCode,
	loginByPhone,
	loginAuthorized,
	loginByUnionid,
}