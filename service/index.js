var http = require('./http.js');

/**
 * 根据地理位置获取学校列表
 * lat
 * lng 
 */
const getSchoolMatch = params => {
  const requestData = {
    url: '/data/school/geo',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取所有学校
 */
const getSchoolList = () => {
  const requestData = {
    url: '/data/school/list',
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 添加地址  获取学校宿舍
const getBuildingList = params => {
  const requestData = {
    url: '/data/school/school/building',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 


/** 
 * 修改用户所在学校 
 * school
 */
const changeSchool = params => {
  const requestData = {
    url: '/data/user/school?school=' + params.school,
    method: 'put',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 获取学校配送配置信息、支付方式、
 * school 
 */ 
const getSchoolConfig = (params) => {
  const requestData = {
    url: '/data/home/school/config',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取学校 Banner、活动列表
 * school 
 * */
const getSchoolData = (params) => {
  const requestData = {
    url: '/data/home/school/data',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取学校 首页为你推荐
 * school 
 * */ 
const getSchoolRecommend = (params) => {
  const requestData = {
    url: '/data/home/recommend',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取学校 首页店铺
 * school 必填
 * lat
 * lng
 * page
 * limit
 * */ 
const getSchoolShop = (params) => {
  const requestData = {
    url: '/data/home/shop',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取学校 首页猜你喜欢
 * school 
 * 
 */ 

const getEstimateShop = (params) => {
  const requestData = {
    url: '/data/home/estimate/shop',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取云闪付活动定义详情（开启状态的）
 * 
 */ 

const getActivityUnionPay = (params) => {
  const requestData = {
    url: '/form/activityUnionPay/getBankPromotion',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取优惠券金额(元)--下单触发满10减9发放优惠券 根据云闪付活动开启状态判断调用
 * 
 */ 
const getUnionPayCoupon = (params) => {
  const requestData = {
    url: '/form/activityUnionPay/getCouponAmount',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}


/**
 * 关闭云闪付优惠券弹窗
 * 
 */ 

const closeUnionPayCoupon = (params) => {
  const requestData = {
    url: '/form/activityUnionPay/closeShowCoupon',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

// 店铺配送信息
const getShopDeliveryType = params => {
  const requestData = {
    url: `/data/home/getFinalDeliveryType?schoolId=${params.schoolId}&shopId=${params.shopId}`,
    method: 'get',
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 公告查询
const shopNotice = params => {
  const requestData = {
    url: '/selective/selNotice/info',
    method: 'get',
	data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 


module.exports = {
	getSchoolMatch,
	getSchoolList,
	getBuildingList,
	getSchoolConfig,
	getSchoolData,
	getSchoolRecommend,
	getSchoolShop,
	changeSchool,
	getEstimateShop,
	getActivityUnionPay,
	getUnionPayCoupon,
	closeUnionPayCoupon,
	getShopDeliveryType,
	shopNotice
}