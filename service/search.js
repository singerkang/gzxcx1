var http = require('./http.js');


/**
 * 获取热门搜素
 * school
 * type   FOOD/HOME
 */ 

const getHotSearch = params => {
  const requestData = {
    url: '/data/search/hot',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 搜索商品
 * school 
 * searchKey 
 * lat  
 * lng 
 * page 
 * limit 
 */ 
const getSearchHomeList = params => {
  const requestData = {
    url: '/data/search/home',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

const getSearchFoodList = params => {
  const requestData = {
    url: '/data/search/food',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



/**
 *  搜索 展示关键字索引列表 
 * lat
 * lng
 * page 
 * limit
 */ 
const getSearchKeyword = params => {
  const requestData = {
    url: '/data/index/school/index/keyword',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


module.exports = {
	getSearchHomeList,
	getSearchFoodList,
	getHotSearch,
	getSearchKeyword,
}