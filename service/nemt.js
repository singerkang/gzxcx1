var http = require('./http.js');

/**
 * 获取活动信息
 */

const getNemtCinfig = params => {
  const requestData = {
    url: '/form/nemt/config',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取vip二维码
 */

const getNemtResult = params => {
  const requestData = {
    url: '/form/nemt/result',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

module.exports = {
	getNemtCinfig,
	getNemtResult
}