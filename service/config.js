var http = require('./http.js');

/**
 * 获取送达时间
 * type  SHOP 商超 FOOD 美食  ERRANDS 跑腿  OFFLINE 线下支付  PERIPHERY
 * store 店铺id
 */ 
const getArrivetTime = (params) => {
  const requestData = {
    url: '/data/appoint/time',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 店铺所有上架商品信息
 * position  SHOP 商超  FOOD 外卖   ERRANDS 跑腿  PERIPHERY 周边  HOME 我的
 */ 
const getBackground = params => {
  const requestData = {
    url: '/data/home/background',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取当前食堂是否限制人数 
 * /data/arrivetime/num
 * params {
 *  canteen 餐厅id
 * }
 * 
 * */
const getArrivetNum = params => {
  const requestData = {
    url: '/data/appoint/num',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 限制预约时间段

const getArrivetLimit = params => {
  const requestData = {
    url: '/data/appoint/limit',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取用户地址列表 
 * user  
 * school
 */ 
const getUserAddressList = params => {
  const requestData = {
    url: '/data/user/address/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 获取用户优惠券
 * state    NOT_USED 我的优惠券 SATISFY 下单查可用优惠券 NEW 新优惠券
 * school
 * store
 * money
 */ 

const getUserOwnCoupon = params => {
  const requestData = {
    url: '/data/coupon',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 获取用户已领取优惠券
const setCouponSee = params => {
  const requestData = {
    url: '/data/coupon/showed?id=' + params,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

/**
 * 根据桌子id 获取桌子信息
 * params: {
 *  table // 桌子id
 * }
 */
const getTableInfo = params => {
  const requestData = {
    url: '/form/eat/code',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 扫码就餐
 * params: {
 *  table // 桌子id
 * }
 */

const scanToEat = params => {
  const requestData = {
    url: '/form/eat/code?table=' + params.table,
    method: 'POST'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

module.exports = {
	getArrivetTime,
	getBackground,
	getUserAddressList,
	getUserOwnCoupon,
	setCouponSee,
	getArrivetNum,
	getArrivetLimit,
	getTableInfo,
	scanToEat
}