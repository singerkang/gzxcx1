var http = require('./http.js');

/**
 * 获取当前进行中的捎饭信息
 * params {
 *  school  学校id
 * }
 */
const getTakeInfo = params => {
  const requestData = {
    url: '/data/take/info',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 发布捎饭
 * params {
 *  appointTime  预计送达时间
 * 	building  宿舍id
 *  buildingName 宿舍名
 *  canteen  食堂id
 *  canteenName  宿舍名称
 *  school 学校id
 * }
 */
const createTakeInfo = params => {
  const requestData = {
    url: '/data/take',
    method: 'POST',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 结束接单
 * params {
 *  recordId  捎饭id
 * }
 */

const takeStop = params => {
  const requestData = {
    url: '/order/take/stop',
    method: 'POST',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 * 取餐
 * params {
 *  recordId  捎饭id
 * }
 */

const takeFetch = params => {
  const requestData = {
    url: '/order/take/fetch',
    method: 'POST',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 * 完成捎饭
 * params {
 *  recordId  捎饭id
 * }
 */

const takeFinish = params => {
  const requestData = {
    url: '/order/take/finish',
    method: 'POST',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取捎饭列表
 * params {
 * 	building  宿舍id
 *  canteen  食堂id
 * }
 */

const getTakeList = params => {
  const requestData = {
    url: '/data/take/list',
    method: 'get',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 *  完成捎饭领取奖励
 * params {
 * 	recordId  捎饭id
 * }
 */

const takeReceive = params => {
  const requestData = {
    url: '/data/take/receive',
    method: 'get',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 *  捎饭记录
 * params {
 * 	page  
 * 	limit  
 * }
 */

const getTakeRecord = params => {
  const requestData = {
    url: '/data/take/record',
    method: 'get',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

module.exports = {
	getTakeInfo,
	createTakeInfo,
	takeStop,
	takeFetch,
	takeFinish,
	getTakeList,
	takeReceive,
	getTakeRecord
}