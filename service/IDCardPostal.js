import {httpRequest} from './baidu.js'
let url = {
	// 获取token
	getToken:'oauth/2.0/token',
	idCard:'rest/2.0/ocr/v1/idcard',
	face: 'rest/2.0/face/v3/person/verify'
	// face: 'rest/2.0/face/v3/match'
}

module.exports = {
	getTokenR(params){
		return httpRequest({
			url:`${url.getToken}?grant_type=client_credentials&client_id=gZ2ZlK5NfNf0jx74p7lxjxkX&client_secret=tLnqgLWWsQ66Q8tOFCzHqwFd3uaNnhk5`
			// url:`${url.getToken}?grant_type=client_credentials&client_id=2UlgFNGaVYemVYzUCm2yCLTp&client_secret=sNVpxxIQacSGE7ghB4ybv8iSUAy8uSay`
			// url:`${url.getToken}?grant_type=client_credentials&client_id=9UCSzZeyTbGpFdTDuzYUNwGy&client_secret=MIBYu5FQCG3ISGMYsIprGc0YE2UURPRl`
		})
	},
	getMessage(params){
		return httpRequest({
			url:`${url.idCard}?access_token=${params.access_token}`,
			data:params.data,
			header:{
				"Content-Type":"application/x-www-form-urlencoded"
			}
		})
	},
	getFaceTokenR(params){
		return httpRequest({
			url:`${url.getToken}?grant_type=client_credentials&client_id=48LKAML6bfxqR6AuDyjljhYA&client_secret=ChHqkG302B35bAkLXRwOD1v562XXszfo`
		})
	},
	postFaceVertify(params){
		return httpRequest({
			url: `${url.face}?access_token=${params.access_token}`,
			data: JSON.stringify(params.data),
			method: 'POST',
			header:{
				"Content-Type":"application/json"
			}
		})
	}
 }
