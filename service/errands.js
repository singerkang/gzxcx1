var http = require('./http.js');


/**
 * 获取重量列表  
 */ 
const getWeightList = () => {
  const requestData = {
    url: '/data/errands/weight/list',
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

/**
 * 根据学校获取录入快递 
 */ 

const getExpressList = params => {
  const requestData = {
    url: '/data/errands/express/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
/**
 * 根据快递点获取地址 
 */ 
const getDistributionList = params => {
  const requestData = {
    url: '/data/errands/distribution/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

module.exports = {
	getWeightList: getWeightList,
	getExpressList: getExpressList,
	getDistributionList: getDistributionList,
}