var http = require('./http.js');

// 邮储信息
const getBankPromotion = params =>{
	const requestData = {
		url: '/form/openAccount/getBankPromotionBySchoolId',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res=>res.data);
};

// 资格验证
const getBankPromotionCheck = params =>{
	const requestData = {
		url: '/form/openAccount/check',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res=>res.data);
};
/**
 * 上传身份证信息
 * params {
 * 
 * }
 */
const uploadIdCard = params => {
  const requestData = {
    url: '/form/openAccount/uploadIdCard',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 无实体介质II类账户开户申请
 * params {
 *  
 * }
 */
const applyDigitalAccount = params => {
  const requestData = {
    url: '/form/openAccount/applyDigitalAccount',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 短信验证码申请
 * params {
 *  
 * }
 */
const applyVerificationCode = params => {
  const requestData = {
    url: '/form/openAccount/applyVerificationCode',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 人脸识别
 * params {
 *  
 * }
 */
const faceRecognition = params => {
  const requestData = {
    url: '/form/openAccount/faceRecognition',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 绑定一类户校验
 * params {
 *  
 * }
 */
const verifyBankCard = params => {
  const requestData = {
    url: '/form/openAccount/verifyBankCard',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 *  七牛上传图片获取token
 * params {
 * 	id   
 * }
 */
const getQiniuToken = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/token?id=' + params.id,
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 *  上传图片
 * params {
 * 	key   
 * }
 */
const uploadImage = params => {
	const requestData = {
		url: '/apiAdmin//qiniu/upload',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

// 银行开户行识别
const verifyBank = params =>{
	const requestData = {
		url: '/form/openAccount/verifyBankNo?bankNo='+params.bankNo,
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
// 交易查询
const tradeResult = params =>{
	const requestData ={
		url: '/form/openAccount/queryTradeResult',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//获取用户账户信息
const getUserAccount = params =>{
	const requestData = {
		url: '/form/bankAccount/getUserAccount',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//充值
const accountRecharge = params =>{
	const requestData ={
		url: '/form/bankAccount/accountRecharge',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//提现
const accountWithdraw = params =>{
	const requestData ={
		url: '/form/bankAccount/accountWithdraw',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//充值交易结果查询
const queryRecharge = params =>{
	const requestData ={
		url: '/form/bankAccount/queryRecharge',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//提现交易结果查询
const queryWithdraw = params =>{
	const requestData ={
		url: '/form/bankAccount/queryWithdraw',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
// 获取充值/提现记录列表
const getRecordList = params =>{
	const requestData = {
		url: '/form/bankAccount/list',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
// 查询用户拉新状态

const searchStatus = params =>{
	const requestData = {
		url: '/form/chinaPay/pullnew?registerMobile='+params.registerMobile,
		method: 'get'
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}


module.exports = {
	getBankPromotion,
	getBankPromotionCheck,
	uploadIdCard,
	applyDigitalAccount,
	applyVerificationCode,
	faceRecognition,
	verifyBankCard,
	getQiniuToken,
	uploadImage,
	verifyBank,
	tradeResult,
	getUserAccount,
	accountRecharge,
	accountWithdraw,
	queryRecharge,
	queryWithdraw,
	getRecordList,
	searchStatus
}