var http = require('./http.js');

/**
 * 根据学校查询秒杀场次
 * params {
 *  school  学校id
 * }
 */
const getKillScene = params => {
  const requestData = {
    url: '/data/kill/scene',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 查询指定场次查询秒杀商品
 * params {
 *  ascId   秒杀场次id
 *  page    分页页数
 *  limit   分页条数
 * }
 */
const getKillGoodsList = params => {
  const requestData = {
    url: '/data/kill/goods/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 查询学校查询拼团商品
 * params {
 *  schoolId   学校
 *  page    分页页数
 *  limit   分页条数
 * }
 */
const getGroupGoods = params => {
  const requestData = {
    url: '/data/group/findGoods',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 查询未成团记录
 * params {
      groupGoodsId
 * }
 */
const getGroupList = params => {
  const requestData = {
    url: '/order/group/query',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 *  /order/group/queryByUser
 *  params {
 *    userid
 *    page
 *    limit
 *  }
 */
const getUserGroup = params => {
  const requestData = {
    url: '/order/group/queryByUser',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 *  /order/group/queryGroupById
 *  params {
 *    groupRecordId
 *  }
 */


const getGroupById = params => {
  const requestData = {
    url: '/order/group/queryGroupById',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 *  根据拼团ID查询商品
 *  params {
 *    goodsId
 *    groupId
 *  }
 */
const getGroupDetail = params => {
  const requestData = {
    url: '/order/group/queryOneGoods',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


module.exports = {
	getKillScene,
	getKillGoodsList,
	getGroupGoods,
	getGroupList,
	getUserGroup,
	getGroupById,
	getGroupDetail
}