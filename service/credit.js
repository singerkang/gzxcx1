var http = require('./http.js');

/**
 * 资格验证
 */

const rechargeCheck = params => {
  const requestData = {
    url: '/form/recharge/check',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 信息登记
 * params {
 * 	name
 * 	phone
 *  address
 *  school
 *  area 号码归属地 CMCC 移动 CTCC 电信 CUCC 联通
 *  operators  所属网路  XIAN  西安   SN 陕西省非西安  OTHER 其他城市
 *  type  参与方式 CARD 办卡 NETWORK 携号转网（非移动号码办理转网）  AREA  归属地迁移
 */

const rechargeRegister = params => {
  const requestData = {
    url: '/form/recharge/register',
    method: 'POST',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 支付
 * params {
			"actRecharge": "string",
			"device": "微信小程序",
			"open_id": "string",
			"payFee": 0,
			"pay_type": "ALIPAY",
			"phone": "string",
			"school": "string",
			"spreadCode": "string",
			"trade_type": "string",
			"type": "REGISTER",
			"user": "string"
 * }
 * 
 */

const rechargePay = params => {
  const requestData = {
    url: '/order/recharge/pay',
    method: 'POST',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}


/**
 * 查询进度
 */

const rechargeProgress = params => {
  const requestData = {
    url: '/form/recharge/progress',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 活动优惠券弹框
 * school
 */

const rechargeCoupon = params => {
  const requestData = {
    url: '/form/recharge/coupon',
    method: 'GET',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 关闭弹框
 */

const rechargeCouponClose = params => {
  const requestData = {
    url: '/form/recharge/coupon',
    method: 'DELETE',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


module.exports = {
	rechargeCheck,
	rechargeRegister,
	rechargePay,
	rechargeProgress,
	rechargeCoupon,
	rechargeCouponClose
}