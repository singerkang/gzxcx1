var http = require('./http.js');

/**
 * 根据学校ID查询学校表单定义
 * params {
 *  schoolId   学校
 *  page    分页页数
 *  limit   分页条数
 * }
 */
const getFormList = params => {
  const requestData = {
    url: '/form/form/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};  
/*
 * 根据学校id 获取开启的可用学校可用表单
 * params {
 *  school   学校
 *  formType   表单类型 1=健康打卡 2=问卷调查
 * }
 */
const getSchoolForm = params => {
  const requestData = {
    url: '/form/form/getSchoolFormBySchoolIdList',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};  
/**
 * 根据表单ID查询表单定义详情
 * params {
 *  id   表单id
 * }
 */
const getFormItem = params => {
  const requestData = {
    url: '/form/form/item',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 根据表单ID查询表单问题定义列表
 * params {
 *  formId   表单id
 * }
 */
const getFormQuestion = params => {
  const requestData = {
    url: '/form/form/question',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 查看详情||根据表单ID、用户ID、日期查询提交记录， 如果有记录则返回问题及答案， 如果没有只返回问题
 */
const gerFormAnswer = params => {
  const requestData = {
    url: '/form/answer/query',
    method: 'Get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * submitAnswer
 */
const submitAnswer = params => {
  const requestData = {
    url: '/form/answer/submit',
    method: 'Post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 问卷统计分析
 * params {
 *  formId   表单id
 * }
 */
const getQuestionnaireReport = params => {
  const requestData = {
    url: '/apiAdmin/form/GetQuestionnaireReport',
    method: 'Get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 可以参与问卷校验
 * params {
 *  formId   表单id
 * }
 */
const getAnswerCheck = params => {
  const requestData = {
    url: '/form/answer/check',
    method: 'Get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



module.exports = {
	getFormList,
	getFormQuestion,
    gerFormAnswer,
    submitAnswer,
	getQuestionnaireReport,
	getFormItem,
	getAnswerCheck,
	getSchoolForm
}