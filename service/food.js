var http = require('./http.js');

/**
 * 根据学校获取背景图片、Banner、为你推荐
 * 
 * school 学校ID
 */ 
///removeGoods   toolingId
const removeGoods = params => {
  const requestData = {
    url: '/removeGoods',
    method: 'DELETE',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///clear 清空购物车
const cleargwc = params => {
  const requestData = {
    url: '/clear',
    method: 'DELETE',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///cutOne 商品信息减-
const cutOne = params => {
  const requestData = {
    url: '/cutOne',
    method: 'put',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///plusOne 商品信息加-
const plusOne = params => {
  const requestData = {
    url: '/plusOne',
    method: 'put',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///info 购物车
const gwcinfo = (params) => {
  const requestData = {
    url: '/info',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
//添加商品 putGoods
const putGoods = params => {
	const requestData = {
		url: '/putGoods',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//http://124.71.202.247:8080/staff/profile 个人信息
const staffprofile = (params) => {
  const requestData = {
    url: '/staff/profile',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
//部门 bumenlist /dept/list
const bumenlist = (params) => {
  const requestData = {
    url: '/dept/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
//机场信息模块 /airport/list
const jclist = (params) => {
  const requestData = {
    url: '/airport/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
///order/{orderId} 删除订单
const delorder = params => {
  const requestData = {
    url: '/order/' + params,
    method: 'delete'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
///order/fetch/{orderId}领取工装
const orderfetch = params => {
  const requestData = {
    url: '/order/fetch/{orderId}orderId?' + params,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
///order/list 订单列表
const getOrders = (params) => {
  const requestData = {
    url: '/order/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
///order/submit 提交订单
const ordersubmit = params => {
	const requestData = {
		url: '/order/submit',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
//activity/list 采购活动列表（移动端）
const activitylist = (params) => {
  const requestData = {
    url: '/activity/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
///tooling/page 分页查询工装列表（小程序端）
const toolingpage = (params) => {
  const requestData = {
    url: '/tooling/page',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
//member/verify用户验证

const memberverify = params => {
	const requestData = {
		url: '/member/verify',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
///toolingType/list 工装分类列表
const toolingTypelist = (params) => {
  const requestData = {
    url: '/toolingType/list',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}
const getFoodHome = (params) => {
  const requestData = {
    url: '/data/food/home',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取首页-店铺列表(sort: score-评分、 monthSale-销量)
 * 
 * school 学校ID
 * sort score-评分、 monthSale-销量
 * page  分页页数
 * limit 分页条数
 * canteen 餐厅ID
 */ 

const getShopList = (params) => {
  const requestData = {
    url: '/data/food/shop',
    method: 'get',
    data: params
  }
  return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 * 获取食堂列表 
 * school
 */

const getCanteen = params => {
  const requestData = {
    url: '/data/school/canteen',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取分类列表 
 * school
 */

const getLabel = params => {
  const requestData = {
    url: '/data/school/label',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 店铺所有上架商品信息
 * store 
 */ 

const getGoodsByStore = params => {
  const requestData = {
    url: '/data/shop/goods/online',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 根据学校获取、Banner、店铺信息
 * school  
 */ 

const getShopInfo = params => {
  const requestData = {
    url: '/data/direct/home',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 根据学校获取、店铺分类信息
 * shop 
 * classify  
 */ 

const getGoodsByClassifies = params => {
  const requestData = {
    url: '/data/shop/goods/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 *  获取店铺优惠券
 */ 
const getStoreCoupon = params => {
  const requestData = {
    url: '/data/coupon/store/' + params,
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

/**
 *  领取店铺优惠券
 */ 
const assignStoreCoupon = params => {
  const requestData = {
    url: '/data/coupon/assign/' + params,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

/**
 *  获取商品详情
 */ 
const getGoodsDirect = params => {
  const requestData = {
    url: '/data/shop/goods/direct/info',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 *  根据商品id获取当日用户已购买商品数量,商品属性-商品限购数量大于0 才调用此接口
 * 	foodId 
 */ 
const getGoodsQuantity = params => {
  const requestData = {
    url: '/order/getTodayBuyQuantityByFoodId',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};





module.exports = {
  removeGoods:removeGoods,
  cleargwc:cleargwc,
  cutOne:cutOne,
  plusOne:plusOne,
  gwcinfo:gwcinfo,
  putGoods:putGoods,
  jclist:jclist,
  staffprofile:staffprofile,
  bumenlist:bumenlist,
  delorder:delorder,
  orderfetch:orderfetch,
  getOrders:getOrders,
  ordersubmit:ordersubmit,
  activitylist:activitylist,
  toolingpage:toolingpage,
  toolingTypelist:toolingTypelist,
	getFoodHome: getFoodHome,
	getShopList: getShopList,
	getCanteen: getCanteen,
	getLabel: getLabel,
	getGoodsByStore: getGoodsByStore,
	getShopInfo: getShopInfo,
	getGoodsByClassifies: getGoodsByClassifies,
	getStoreCoupon: getStoreCoupon,
	assignStoreCoupon: assignStoreCoupon,
	getGoodsDirect: getGoodsDirect,
	getGoodsQuantity:getGoodsQuantity
}