var http = require('./http.js');


/**
 * 发布	
 * params {
 * }
 */

const postPromotionRecord = params => {
  const requestData = {
    url: '/form/promotionRecord/item',
    method: 'Post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 *  资格验证
 * params {
 * 	id 光盘活动定义id  
 * }
 */

const getPromotionCheck = params => {
  const requestData = {
    url: '/form/promotionRecord/check',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 *  根据学校Id获取光盘活动定义详情（开启状态的）
 * params {
 * 	school  学校id  
 * }
 */
const getPromotionRecord = params => {
	const requestData = {
		url: '/form/promotionRecord/getSchoolPromotionBySchoolId',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}


/**
 *  用户参与情况 累计打卡天数
 * params {
 * 	schoolPromotionId  光盘活动定义Id  
 * }
 */
const getUserPromotionRecord = params => {
	const requestData = {
		url: '/form/promotionRecord/getUserPromotionRecord',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}



/**
 *  根据参与记录Id 获取参与详情
 * params {
 * 	id   id  
 * }
 */
const getPromotionRecordItem = params => {
	const requestData = {
		url: '/form/promotionRecord/item',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}



/**
 *  用户参与情况 累计打卡天数 审核情况 优惠券获得情况
 * params {
 * 	id   参与记录id  
 * }
 */
const deletePromotionRecordItem = params => {
	const requestData = {
		url: '/form/promotionRecord/item?id=' + params.id,
		method: 'delete',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}



/**
 *  参与列表
 * params {
 * 	schoolPromotionId   光盘活动定义Id
 *  userId  用户id
 *  page   页码
 *  limit  页尺寸
 * }
 */
const getPromotionRecordList = params => {
	const requestData = {
		url: '/form/promotionRecord/list',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
/**
 *  个人参与打卡列表 按时间排序
 * params {
 * 	schoolPromotionId   光盘活动定义Id
 *  userId  用户id
 *  page   页码
 *  limit  页尺寸
 * }
 */
const getMyPromotionRecordList = params => {
	const requestData = {
		url: '/form/promotionRecord/my',
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 *  七牛上传图片获取token
 * params {
 * 	id   
 * }
 */
const getQiniuToken = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/token?id=' + params.id,
		method: 'get',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

/**
 *  删除图片
 * params {
 * 	key   
 * }
 */
const deleteQiniuImage = params => {
	const requestData = {
		url: '/apiAdmin/qiniu/delete?key='+params.key,
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}
/**
 *  上传图片
 * params {
 * 	key   
 * }
 */
const uploadImage = params => {
	const requestData = {
		url: '/apiAdmin//qiniu/upload',
		method: 'post',
		data: params
	};
	return http.fetchHeaderApi(requestData).then(res => res.data);
}

module.exports = {
	postPromotionRecord,
	getPromotionCheck,
	getPromotionRecord,
	getUserPromotionRecord,
	getPromotionRecordItem,
	deletePromotionRecordItem,
	getPromotionRecordList,
	getQiniuToken,
	deleteQiniuImage,
	getMyPromotionRecordList,
	uploadImage
}