var http = require('./http.js');

/**
 * 获取当前学校分享送优惠券活动
 * school
 */ 

const getSpreadCoupon = params => {
  const requestData = {
    url: '/data/spread/coupon',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 获取当前学校分享送优惠券活动
 * school
 */ 
const getSpreadActivity = params => {
  const requestData = {
    url: '/data/spread/activity',
    method: 'get',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};

/**
 * 接收者打开页面获得优惠券
 * spread
 * send
 * code
 */ 
const spreadCouponReceive = params => {
  const requestData = {
    url: '/data/spread/receive',
    method: 'post',
    data: params
  };
  return http.fetFormHeaderApi(requestData).then(res => res.data);
};


module.exports = {
	getSpreadCoupon,
	getSpreadActivity,
	spreadCouponReceive
}