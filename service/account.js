var http = require('./http.js');

// 明细
const getAccountDetails = params => {
  const requestData = {
    url: '/selective/selMemberAccountRec/pageById',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 账户信息
const getShopAccount = params => {
  const requestData = {
    url: '/selective/selMemberAccount/info',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 设置提现密码
const setPassword = params => {
  const requestData = {
    url: '/selective/selMemberAccount/setPassword?password='+params.password+'&phone='+params.phone+'&userId='+params.userId+'&code='+params.code,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

// 提现
const withdrawal = params => {
  const requestData = {
    url: '/selective/selMemberAccount/cush',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 


module.exports = {
	getAccountDetails,
	setPassword,
	getShopAccount,
	withdrawal
}

