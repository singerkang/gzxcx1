var http = require('./http.js');

// 获取用户地址列表 
const getUserAddressList = params => {
  const requestData = {
    url: '/data/user/address/list',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};  

// 添加 更新 收货地址
const updateAddress = params => {
  const requestData = {
    url: '/data/user/address/save',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 
// 删除收货地址

const delAddress = params => {
  const requestData = {
    url: '/data/user/address/' + params,
    method: 'delete'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
}; 

module.exports = {
	getUserAddressList,
	updateAddress,
	delAddress,
}