var http = require('./http.js');

/**
 * 查找优选商品
 * params {
 *  name  name
 *  id    id
 * }
 */

const getSearchGoods = params => {
  const requestData = {
    url: '/selective/selGoods/searchByKey',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 


/**
 * 默认店铺
 * params {
 *  userId  userId
 *  shopId    shopId
 * }
 */

const getDefShop= params => {
  const requestData = {
    url: '/selective/selShop/defShop',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};
/**
 * 附近店铺
 * params {
 *  currentLat  经度
 *  currentLon    纬度
 *  distance 附近多少公里
 *  page 
 *  limit
 * }
 */
const getNearShop= params => {
  const requestData = {
    url: '/selective/selShop/nearByShopPage',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 商品
 * params {
 *  name  名称
 *  goodsType    商品分类
 *  id      id
 *  memberId  用户id
 *  page 
 *  limit
 * }
 */
const getSelGoods= params => {
  const requestData = {
    url: '/selective/selGoods/searchByKey',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 商品详情
 * params {
 *  id      id
 * }
 */
const getSelGoodsDetail= id => {
  const requestData = {
    url: '/selective/selGoods/searchById?id='+id,
    method: 'get',
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


// 获取分类 无参
const getTypeList= params => {
  const requestData = {
    url: '/selective/selGoodsType/getList',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


// 购物车数量 userId
const getCartNum= id => {
  const requestData = {
    url: '/selective/selCart/cartNum?userId='+id,
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

// 购物车列表 userId
const getCart= id => {
  const requestData = {
    url: '/selective/selCart/list?userId='+id,
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 首页加购物车
 * params {
 *  goodsId  商品id
 *  goodsNum   添加数量
 *  memberId  用户id
 * }
 */
const addCart= params => {
  const requestData = {
    url: '/selective/selCart/add',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 订单 待付款0 待提货2 全部4
 * params {
 *  memberId  memberId
 *  status    status
 *  page 
 *  limit
 * }
 */
const getOrders= params => {
  const requestData = {
    url: '/selective/selOrder/searchByMemberId',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 系统当前时间
 */
const getNowDate= params => {
  const requestData = {
    url: '/selective/selGoods/nowDate',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 退款订单
 * params {
 *  memberId  memberId
 *  page 
 *  limit
 * }
 */
const getRefundOrders= params => {
  const requestData = {
    url: '/selective/selOrder/refundOrderByMemberId',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



/**
 * id查订单详情
 * params {
	 * orderId 
	* }
 */
const getOrderDetail= id => {
  const requestData = {
    url: '/selective/selOrder/infoByOrderId?orderId='+id,
    method: 'get'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 店铺id查距离
 * params {
	 * shopId  lat lon 
	* }
 */
const getOrderDistance= params => {
  const requestData = {
    url: '/selective/selShop/searchDistanceByShopId?shopId='+params.shopId+'&lat='+params.lat+'&lon='+params.lon,
    method: 'get',
	data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 提交订单
 * params {
 * }
 */
const confirmOrder= params => {
  const requestData = {
    url: '/selective/selOrder/orderSubmit',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 删除购物车
 * params {
	 * memeberId 
	 * goodsIdList 
 * }
 */
const deleteCart= params => {
  const requestData = {
    url: '/selective/selCart/delete?memeberId='+params.memeberId+'&goodsIdList='+params.goodsIdList,
    method: 'put'
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 删除订单
 * params {
	 * orderNo  整个订单删除只传orderNo 某个商品要传goodsId
	 * goodsId 
 * }
 */
const deleteOrder= params => {
  const requestData = {
    url: '/selective/selOrder/deleted?orderNo='+params.orderNo+'&goodsId='+params.goodsId,
    method: 'put',
	data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 取消订单
 * params {
	 * orderNo  整个订单取消只传orderNo 某个商品要传goodsId
	 * goodsId 
 * }
 */
const cancelOrder= params => {
  const requestData = {
    url: '/selective/selOrder/cancel?orderNo='+params.orderNo+'&goodsId='+params.goodsId,
    method: 'put',
	data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};

/**
 * 确认提货
 * params {
	 * orderNo 
 * }
 */
const receiveOrder= orderNo => {
  const requestData = {
    url: '/selective/selOrder/receivedByUser?orderNo='+orderNo,
    method: 'put',
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



/**
 * 用户账户信息
 * params {
 *  memberId  memberId
 * }
 */
const getUserAccount= params => {
  const requestData = {
    url: '/selective/selMemberAccount/info',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 用户id查看流水
 * params {
 *  id  id
 * page page
 * limit limit
 * }
 */
const getUserAccountRec= params => {
  const requestData = {
    url: '/selective/selMemberAccountRec/pageById',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};




/**
 * 查看商品活动列表 无参数
 */
const getActList= params => {
  const requestData = {
    url: '/selective/selAct/actList',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 查看商品活动详情
 * params {
 *  id  actId
 * }
 */
const getActGoodsDetails= id => {
  const requestData = {
    url: '/selective/selAct/selectById?id='+id,
    method: 'get',
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 查看活动的商品
 * params {
 *  actId  actId
 * limit
 * page
 * }
 */
const getGoodsAct= params => {
  const requestData = {
    url: '/selective/selAct/actGoodsPage',
    method: 'get',
	data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};


/**
 * 订单支付
 * params {
 * app_id  
 * client_ip
 * device
 * open_id
 * orderNo
 * payFee
 * pay_type
 * trade_type
 * }
 */
const orderPay= params => {
  const requestData = {
    url: '/selective/selOrder/pay',
    method: 'post',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



/**
 * 前端支付完成回调通知
 * params {
 * }
 */
const orderPayNotice= params => {
  const requestData = {
    url: '/selective/selOrder/notice',
    method: 'get',
    data: params
  };
  return http.fetchHeaderApi(requestData).then(res => res.data);
};



module.exports = {
	getSearchGoods,
	getDefShop,
	getNearShop,
	getSelGoods,
	getTypeList,
	getCartNum,
	getCart,
	addCart,
	confirmOrder,
	deleteCart,
	getSelGoodsDetail,
	getOrders,
	getUserAccount,
	getActList,
	getGoodsAct,
	getUserAccountRec,
	getOrderDetail,
	getRefundOrders,
	cancelOrder,
	deleteOrder,
	getNowDate,
	orderPay,
	orderPayNotice,
	getOrderDistance,
	receiveOrder,
	getActGoodsDetails
}