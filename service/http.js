// 环境 
// const testHost = 'https://i.gxdst.cn'; //测试环境
const testHost = 'http://124.71.202.247:8080'; //测试环境
// const testHost = 'https://api1.xasqkj.com/'; //测试环境
// const testHost = 'http://192.168.10.141:8080'; //测试环境

// const testHost = 'http://192.168.1.26:8700'; //测试环境 刘艳利
// const testHost = 'http://192.168.1.10:8700'; //测试环境 孔
// const testHost = 'http://192.168.1.12:8700'; //测试环境 帅
const formalHost = 'https://api.gxdst.cn'; //生产环境

const isTest = true;
  // const isTest = false;

const version = '2.0';

const host = isTest ? testHost : formalHost;
const Rsasign = require("../utils/rsasign.js"); // 签名
const Util = require("../utils/util.js"); 

const fetFormHeaderApi = params => {
  const app = getApp().globalData;
  const access_token = getApp().globalData.userInfo.access_token ? getApp().globalData.userInfo.access_token : '';

	let headerParams = {
		"Content-Type": "application/x-www-form-urlencoded",
		"Authorization": uni.getStorageSync('token')
	}
	
	if (params.method.toLowerCase() != 'get') {
		
		const nowTime = new Date().getTime();
		const signature = Rsasign.createSign(params.data, nowTime);
		headerParams.timestamp = nowTime;
		headerParams.version = version;
		headerParams.signature = signature;
		headerParams.Origin = 'xa87-wechat-mini';
	}
	return new Promise((resolve, reject) => {
      uni.request({
        url: host + params.url,
        data: params.data,
        method: params.method,
        header: headerParams,
        success: resolve,
        fail: function () {
					reject({ data: {errorMessage: '网络错误', status: 'FAILED'} });
				}
      });
  });
};

const fetchHeaderApi = params => {
  const app = getApp().globalData;
  const access_token = getApp().globalData.userInfo.access_token ? getApp().globalData.userInfo.access_token : '';
	
	let headerParams = {
		"Authorization": uni.getStorageSync('token')
	}
	
	if (params.method.toLowerCase() != 'get') {
		const nowTime = new Date().getTime();
		const signature = Rsasign.createSign(params.data, nowTime);
		headerParams.timestamp = nowTime;
		headerParams.version = version;
		headerParams.signature = signature;
		headerParams.Origin = 'xa87-wechat-mini';
	}
  return new Promise((resolve, reject) => {
		uni.request({
			url: host + params.url,
			data: params.data,
			method: params.method,
			header: headerParams,
			success: resolve,
			fail: function () {
				reject({ data: {errorMessage: '网络错误', status: 'FAILED'} });
			}
		});
  });
};

const loginApi = params => {
	const nowTime = new Date().getTime();
	const signature = Rsasign.createSign(params.data, nowTime);
	
  return new Promise((resolve, reject) => {
    uni.request({
      url: host + params.url,
      data: params.data,
      method: params.method,
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Basic dGFyb2NvOnRhcm9jbw==",
				timestamp: nowTime,
				version: version,
				signature: signature,
				Origin: 'xa87-wechat-mini'
      },
      success: resolve,
      fail: function () {
				reject({ data: {errorMessage: '网络错误', status: 'FAILED'} });
			}
    });
  });
}; 


module.exports = {
	fetchHeaderApi: fetchHeaderApi,
	fetFormHeaderApi: fetFormHeaderApi,
	loginApi: loginApi
}