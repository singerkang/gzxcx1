<p align="center">
    <img alt="logo" src="static/images/img/logo11.png" width="120" height="120" style="margin-bottom: 10px;">
</p>
<h3 align="center" style="margin: 30px 0 30px;font-weight: bold;">小程序</h3>
<h3 align="center">吉祥世家工装定制</h3>
		
## 目录说明
[TOC]

	|-common      第三方SDK
	|-components  uni-app组件目录，可复用的组件
	|-page  	  存放整个uniapp所有的页面
		|-login     微信一键授权
		|-Authorization 用户认证
		|-index   		  首页
		|-ShopCart        购物车
		|-mine           个人中心
	|-static   	  存放所有的静态资源
	|-unpackage   存放打包输出文件
	|-uview-ui    Uview框架组件
	|-APP.vue     项目根组件【调用应用的生命周期函数】	
	|-main.js     项目的入口函数【项目首先加载】
	|-mainifest.json  配置appid等打包所需的东西
	|-page.json   设置整个项目的存放路径和窗口外观
	|-README.md   项目说明
	|-uni.scss    常用样式变量

## 项目运行说明

1. HbuildX 导入项目

2. 下载安装微信开发者工具（[微信小程序API](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)）

2. 运行>运行到小程序模拟器>微信开发者工具

## 项目上传发布说明

1. 微信开发者工具>上传（版本号，说明） 

2. 登录（[微信公众平台](https://mp.weixin.qq.com/)）

3. 进入版本管理，项目发布审核上线或发布体验版本


## 链接

- [UniApp官方文档](https://uniapp.dcloud.io/api/README)
- [Uview官方文档](https://uviewui.com/components/intro.html)
