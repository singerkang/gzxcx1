/*
 * 数据埋点
 */
const app = getApp();

const track = (eventName, parame) => {
  const attribute = parame || {};
  attribute['学校ID'] = '';
  attribute['学校名称'] = '未知学校';
  const schoolInfo = app.globalData.schoolInfo;
  const userInfo = app.globalData.userInfo;

  if (schoolInfo && schoolInfo.id) {
    attribute['学校ID'] = schoolInfo.id;
    attribute['学校名称'] = schoolInfo.name;
  }
	if (eventName == '进入系统' && userInfo.user_info) {
		attribute['学校ID'] = userInfo.user_info.school;
		attribute['学校名称'] = userInfo.user_info.schoolName;
	}
	
	// #ifdef MP-WEIXIN
	attribute['平台'] = '微信小程序';
	// #endif
	// #ifdef MP-QQ
	attribute['平台'] = 'QQ小程序';
	// #endif
	console.log(eventName);
	console.log(attribute);
	if (uni.canIUse("getNetworkType")) {
		// #ifdef MP
		try{
			App.zhuge.track(eventName, attribute);
		}catch(e){
			//TODO handle the exception
			console.log('诸葛IO异常');
		}
		// #endif
	}
	
};


const identify = (eventName, parame) => {
  const attribute = parame || {};
	if (uni.canIUse("getNetworkType")) {
		// #ifdef MP
		try{
			App.zhuge.identify(eventName, attribute);
		}catch(e){
			console.log('诸葛IO异常');
		}
		// #endif
	}
};

module.exports = {
	track,
  identify
};