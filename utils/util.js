/**
 * 时间格式化 
 */
const formatTime = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':');
};

const formatDate = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  return [year, month, day].map(formatNumber).join('-');
};

const formatNumber = n => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};
/**
 * 时间倒计时 
 */


const timeCountdown1 = data => {
  const endTiem = new Date(formatTime(new Date()).replace(/-/g, '/').split(' ')[0] + ' ' + data).getTime();
  const nowTiem = new Date().getTime();
  const alltime = endTiem - nowTiem; //总的时间（毫秒）

  if (alltime > 0) {
    const scend = parseInt(alltime / 1000 % 60); //秒

    const minute = parseInt(alltime / 1000 / 60 % 60); //  分钟

    const hour = parseInt(alltime / 1000 / 60 / 60 % 24); //小时

    const day = parseInt(alltime / 1000 / 60 / 60 / 24 % 30); //天数

    const month = parseInt(alltime / 1000 / 60 / 60 / 24 / 30 % 12); //月

    return [hour, minute, scend].map(formatNumber);
  } else {
    return false;
  }
};

const timeCountdown = data => {
  if (data > 0) {
    const scend = parseInt(data / 1000 % 60); //秒

    const minute = parseInt(data / 1000 / 60 % 60); //  分钟

    const hour = parseInt(data / 1000 / 60 / 60 % 24); //小时

    return [hour, minute, scend].map(formatNumber);
  } else {
    return false;
  }
};
const timeCountdown2 = data => {
  if (data > 0) {
    const scend = parseInt(data / 1000 % 60); //秒

    const minute = parseInt(data / 1000 / 60 % 60); //  分钟


    return [minute, scend].map(formatNumber);
  } else {
    return false;
  }
};
/**
 * 检查微信会话是否过期
 */


function checkSession() {
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        resolve(true);
      },
      fail: function () {
        reject(false);
      }
    });
  });
}
/**
 * 调用微信登录
 */


function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          resolve(res.code);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

function getUserInfo() {
  return new Promise(function (resolve, reject) {
    wx.getUserInfo({
      withCredentials: true,
      lang: "zh_CN",
      success: function (res) {
        if (res.errMsg === 'getUserInfo:ok') {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
} 

// 生成n个随机数
function randomn(n) {
  if (n > 21) return null;
  return parseInt((Math.random() + 1) * Math.pow(10, n - 1));
}


module.exports = {
  formatTime,
	formatDate,
  timeCountdown,
  timeCountdown1,
  checkSession,
  login,
  getUserInfo,
  randomn,
  timeCountdown2
};