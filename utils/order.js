/**
 * 订单相关
 */
const orderApi = require("../service/order.js");

const app = getApp().globalData; //  支付订单

const toPayOrder = orderID => {
  uni.navigateTo({
    url: '/pages/payOrder/payOrder?orderID=' + orderID
  });
};

// 删除订单
const orderDelete = orderID => {
	return new Promise((resolve, reject) => {
		uni.showModal({
		  title: '提示',
		  content: '确定删除订单？',
		  success: res => {
		    if (res.confirm) {
		      uni.showLoading({
		        title: '删除中',
		      });
		      const requestData = {
		        orderNo: orderID
		      };
		      orderApi.orderDelete(requestData).then(res => {
						uni.hideLoading();
						if (res.status == 'SUCCEED') {
							uni.showToast({
								title: '删除成功',
								icon: 'none'
							});
							resolve(res);
						} else {
							uni.showToast({
								title: res.errorMessage,
								icon: 'none'
							});
						}
		      });
		    }
		  }
		});
	}).catch(e => {});
};

// 急速退款
const refundFast = order => {
  return new Promise(function (resolve, reject) {
    uni.showModal({
      title: '提示',
      content: '确定取消订单？',
      success: res => {
        if (res.confirm) {
          uni.showLoading({
            title: '取消中'
          });
          const requestData = {
            orderNo: order.order_no,
						type: order.type
          };
          orderApi.orderRefundFast(requestData).then(res => {
						uni.hideLoading();
						if (res.status == 'SUCCEED') {
							uni.showToast({
								title: '退款成功',
								icon: 'none'
							});
							resolve(res);
						} else {
							uni.showToast({
								title: res.errorMessage,
								icon: 'none'
							});
						}

          });
        }
      }
    });
  }).catch(e => {});
}; 

// 再来一单
const regenerateOrder = order => {
  const store = order.store;
  console.log('order.type',order.type)
  if (order.type == 'PERIPHERY') {
		
		let goods = order.goods;
		if(order.goods.constructor == String) {
			goods = JSON.parse(goods);
		}
		uni.navigateTo({
			url: `/packagesAround/pages/aroundDetail/aroundDetail?storeId=${order.store}`
		})
		return;
	}  
	// 外卖
	let goodsList = order.goods;
	if(goodsList.constructor == String) {
		goodsList = JSON.parse(goodsList);
	}
	const obj = {};
	
	for (let i = 0; i < goodsList.length; i++) {
		let limitQuantity = goodsList[i].limitQuantity;
		let attrIds = goodsList[i].attrIds;

		if (attrIds) {
			attrIds = attrIds.replace(/,/g, '_');
		}
		const cartId = goodsList[i].foodId + (attrIds ? '_' + attrIds : '');
		if(limitQuantity == 0){
			obj[cartId] = {
				...goodsList[i],
				cartId
			};
		}
	}
	console.log('再来一单',obj)
	getApp().globalData.regenerateList = obj;
	if (order.type == 'SHOP') {
		uni.navigateTo({
		  url: '/pages/businessSuper/index?show=1'
		});

	} else {
		// 美食 FOOD
		let goods = goodsList;
		let goods2 = [];
		goods.forEach(v=>{
			if(v.limitQuantity == 0 ){
				goods2.push(v);
			}
		})
		order.goodsCart= goods2;
		if(goods2.length > 0){
			wx.navigateTo({
				url: '/pages/store/store?storeId=' + store + '&show=1'
			});
		}else{
			wx.navigateTo({
				url: '/pages/store/store?storeId=' + store + '&show=0'
			});
		}
	}

}; 

module.exports = {
  toPayOrder,
  refundFast,
  regenerateOrder,
	orderDelete,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
};