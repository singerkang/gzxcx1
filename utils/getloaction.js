const getUserLocation = () => {
  return new Promise((resolve, reject) => {
		// #ifdef H5
		geo().then(res => {
		  resolve(res);
		}).catch(res => {
		  reject(res);
		});
		// #endif
		// #ifdef MP
    uni.getSetting({
      success: res => {
        // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
        // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
        // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          //未授权
          uni.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                //取消授权
                uni.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                });
                reject(res);
              } else if (res.confirm) {
                //确定授权，通过uni.openSetting发起授权请求
                uni.openSetting({
                  success: function (res) {
                    if (res.authSetting["scope.userLocation"] == true) {
                      uni.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      }); //再次授权，调用uni.getLocation的API

                      geo().then(res => {
                        resolve(res);
                      }).catch(res => {
                        reject(res);
                      });
                      ;
                    } else {
                      uni.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      });
                    }
                  }
                });
              }
            }
          });
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //用户首次进入页面,调用uni.getLocation的API
          geo().then(res => {
            resolve(res);
          }).catch(res => {
            reject(res);
          });
          ;
        } else {
          //  授权成功  调用uni.getLocation的API
          geo().then(res => {
            resolve(res);
          }).catch(res => {
            reject(res);
          });
        }
      },
      fail: res => {
        reject(res);
      }
    });
		// #endif
  });
}; // 获取定位城市


const geo = () => {
  return new Promise((resolve, reject) => {
    let address = {};
    uni.getLocation({
      type: 'wgs84',
      success: function (res) {
        const coord = gcj02tobd09(res.latitude, res.longitude);
        address = {
          lat: coord[0],
          lng: coord[1]
        };
        resolve(address);
      },
      cancel: function (res) {
        reject(res);
      },
      fail: function (res) {
				uni.hideLoading();
				uni.showModal({
					title: '提示',
					showCancel: false,
					content: '请在系统设置中打开定位服务',
					confirmText: '确定',
					success: function (res) {
					}
				})
        reject(res);
      }
    });
  });
}; // 腾讯地图坐标 转 百度地图坐标


function gcj02tobd09(lat, lng) {
  const x_PI = 3.14159265358979324 * 3000.0 / 180.0;
  const z = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_PI);
  const theta = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_PI);
  const bd_lng = z * Math.cos(theta) + 0.0065;
  const bd_lat = z * Math.sin(theta) + 0.006;
  return [bd_lat, bd_lng];
}



module.exports = {
  getUserLocation: getUserLocation
};