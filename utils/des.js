
import CryptoJS from '../node_modules/crypto-js/crypto-js.js'
// DES加密
const encryptDes = (message, key) => {
	var keyHex = CryptoJS.enc.Utf8.parse(key);
	var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
		mode: CryptoJS.mode.ECB,
		padding: CryptoJS.pad.Pkcs7
	});
	return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
}
 
// DES解密
const decryptDes = (ciphertext, key) => {
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const decrypted = CryptoJS.DES.decrypt({
     ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
   }, keyHex, {
     mode: CryptoJS.mode.ECB,
     padding: CryptoJS.pad.Pkcs7
  });
  return decrypted.toString(CryptoJS.enc.Utf8);
}

module.exports = {
  encryptDes,
  decryptDes,
};